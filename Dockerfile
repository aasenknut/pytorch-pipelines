## Train model
FROM python:3.8 AS trainer

RUN apt-get -y update && apt-get install -y --no-install-recommends \
         wget \
         ca-certificates \
    && rm -rf /var/lib/apt/lists/*


COPY requirements.txt /tmp
WORKDIR /tmp
RUN pip install --upgrade pip && \
    pip install -r requirements.txt

COPY train /program/train
COPY input /program/input
COPY data /program/data
COPY utils /program/utils
COPY config /program/config
COPY run_trainer.py /program/
WORKDIR /program
RUN ls -l
RUN python run_trainer.py

## Second build stage: Build app for deployment with trained model from previous
FROM python:3.8 AS app

RUN apt-get -y update && apt-get install -y --no-install-recommends \
         wget \
        ca-certificates \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /tmp
WORKDIR /tmp
RUN pip install --upgrade pip && \
    pip install -r requirements.txt

COPY app /program/app
COPY data /program/data
COPY config /program/config
COPY utils /program/utils
COPY run_app.sh /program/
COPY --from=trainer /program/models /program/models
WORKDIR /program
ENTRYPOINT ["sh", "run_app.sh"]
