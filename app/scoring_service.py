import joblib

from utils.logging import get_logger
from config.model_config import ModelConfig
from config.input_config import InputConfig


logger =  get_logger(level="INFO", name="__name__")


class ScoringService:
    """

    Attributes:
        model (torch object):
         (): 
    """
    
    model = joblib.load()

    @classmethod
    def score(cls, features):
        try:
            ret = cls.model.predict_proba(features)
        except Exception as err:
            logger.info(
                "Following exception occurd while making prediction:"
                + f"{err.__class__}"
            )
            raise err
        return ret


