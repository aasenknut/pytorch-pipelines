import time

import pandas as pd

from utils.logging import get_logger
from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from app.scoring_service import ScoringService


logger = get_logger(level="INFO", name="__name__")
app = FastAPI()


@app.get("/ping")
def ping():
    """
    Determine if the app is healthy.

    We say that the app is healthy if the model loads properly.
    """
    logger.info("Ping!")
    if ScoringService.model is not None:
        status = 200
    else:
        status = 400
    return JSONResponse(status_code=status)

@app.post("/score")
def score(request):
    """
    Scores a payload.

    Args:
        payload (JSON): Meta data and features.
    Returns:
        JSON object with prediction result and the meta data.
    """
    try:
        df = pd.DataFrame([request.features])
    except:
        err = "Couldn't transform the features into a pandas.DataFrame"
        raise HTTPException(status_code=404, detail=err)

    logger.info("Starting scoring.")
    tic = time.perf_counter()
    try:
        result = ScoringService.score(df)
        logger.info(f"Result from scoring service: {result}")
    except:
        err = "Couldn't score the given features."
        raise HTTPException(status_code=404, detail=err)
    toc = time.perf_counter()
    logger.info(f"Scoring duration: {toc - tic: 0.4f} seconds.")

    ret = {
        "prediction": score,
        "meta": request.meta.dict(),
    }
    logger.info(f"Return value from scoring service: {ret}")
    return ret
