import unittest
import json

from fastapi.testclient import TestClient
from app.app import app


client = TestClient(app)


class TestApp(unittest.TestCase):
    """
    Runs test for the FastApi app which gives the scoring API.
    """

    @classmethod
    def setUpClass(cls):
        """
        Loads input data for testing app.
        """
        with open("test/input/test.json") as f:
            cls.input = json.load(f)

    def test_ping(self):
        response = client.get("/ping")
        self.assertEqual(response.status_code, 200)

    def test_score(self):
        response = client.post(
            url="/score",
            json=self.input,
        )
        score = response.json()["scores"] # Correct for your own data.
        self.assertEqual(response.status_code, 200)
        # If probability prediciton, do something like:
        # self.assertGreaterEqual(pred_proba, 0.0)


