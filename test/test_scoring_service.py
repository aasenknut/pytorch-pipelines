import unittest

import pandas as pd

from app.scoring_service import ScoringService


class TestScoringService(unittest.TestCase):
    """
    Tests the scoring service which is used by the scoring app.
    """

    @classmethod
    def setUpClass(cls):
        """
        Loads the scoring service as well as input data for testing the scoring service.
        """
        cls.scoring_service = ScoringService()
        cls.input = pd.read_csv(INPUT_PATH)

    def test_scoring(self):
        """
        Tests that the scoring service functions properly.
        """
        score = self.scoring_service.predict(self.input)
        # Perform relevant tests on the score...
