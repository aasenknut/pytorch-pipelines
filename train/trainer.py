import torch
import math
from torch import nn
from torch.utils.data import DataLoader
from configs.model_config import ModelConfig


class Trainer:

    def __init__(self, model, config):
        self.model = model
        self.config = config
        self.opt = self._get_optimizer()
        self.loss = nn.BCELoss()
        self.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu"
        )
        self._running_loss = None

    def _get_optimizer(self):
        if self.config.optimizer == "rmsprop":
            opt = torch.optim.RMSprop(self.model.parameters(), lr=self.config.lr)
        elif self.config.optimizer == "adadelta":
            opt = torch.optim.Adadelta(self.model.parameters(), lr=self.config.lr)
        else:
            opt = torch.optim.Adam(params=self.model.parameters(), lr=self.config.lr)
        return opt

    def _cycle_lr(self, epoch_idx: int):
        """
        Cycles learning rate based on the epoch index.
        The cycling strategy is stochastic gradient descent with warm restarts.

        Args:
            epoch_idx: current epoch
        """
        T_curr = epoch_idx % self.config.sgdr_T_max
        lr_updated = (
            self.config.sgdr_eta_min
            + 0.5
            * (self.config.sgdr_eta_max - self.config.sgdr_eta_min)
            * (1 + math.cos(math.pi * T_curr / self.config.sgdr_T_max))
        )
        for param_group in self.opt.param_groups:
            param_group['lr'] = lr_updated
        return self

    def _run_step(self, features_, target_):
        self.opt.zero_grad()
        preds = self.model(features_).squeeze()
        loss_step = self.loss(preds, target_)
        self._running_loss = loss_step
        loss_step.backward()
        self.opt.step()
        return self

    def _run_epoch(self):
        for features, target in self.dataloader:
            features = features.to(self.device, dtype=torch.float)
            target = target.to(self.device, dtype=torch.float)
            self._run_step(features_=features, target_=target)
        return self

    def fit(self, dataset):
        """
        Args:
            dataset (torch.utils.data.Dataset): used for training network.
        """
        self.dataloader = DataLoader(
            dataset=dataset,
            batch_size=self.config.batch_size
        )
        self.model.train()
        for epoch_idx in range(self.config.n_epochs):
            self._run_epoch()
            if epoch_idx > self.config.epoch_start_lr_cycle:
                self._cycle_lr(epoch_idx)
        return self

    def predict(self, features):
        """
        Args:
            features (numpy.array): features to predict on.
        Returns:
            Array of predictions.
        """
        self.model.eval()
        preds = self.model(features).squeeze()
        ret = preds.detach().numpy()
        return ret

    def write(self, save_path):
        """
        Writes net to file.
        """
        torch.save(self.model.state_dict(), save_path)
