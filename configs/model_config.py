"""
Config set for training NNs with PyTorch
"""


class ModelConfig:
    input_size = 10
    output_size = 1
    lin1_size = 15
    dropout_p = 0.1


class TrainerConfig:
    lr = 0.001
    optimizer = "adam"
    network = "mlp"
    batch_size = 3
    n_epochs = 10
    lr = 0.001
    epoch_start_lr_cycle = 12
    sgdr_T_max = 3
    sgdr_eta_min = 0.001
    sgdr_eta_max = 0.01
    torch_save_path = "output/network.pt"

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
